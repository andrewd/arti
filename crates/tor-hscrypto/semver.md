ADDED: New APIs for order-preserving encryption.
ADDED: Into/From conversions between curve25519::StaticKeyPair and HsSvcNtorKey
ADDED: Into/From conversions from curve25519::StaticKeyPair to HsClientDescEncKey
DEPRECATED: `HsClientIntroAuthKey`, `HsClientIntroAuthKeypair`
