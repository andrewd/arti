ADDED: StreamParameters::suppress_{flags,hostname}
MODIFIED (experimental): Add `PendingClientCirc::create_firsthop_ntor_v3`
MODIFIED (experimental): Add `ClientCirc::extend_ntor_v3`
